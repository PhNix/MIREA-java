package forth;

public class Human {
    private Head head;
    private Hand rightHand;
    private Hand leftHand;
    private Leg rightLeg;
    private Leg leftLeg;

    public Human() {
        this.head = new Head("black", "black");
        this.rightHand = new Hand(false, true);
        this.leftHand = new Hand(false, false);
        this.rightLeg = new Leg(false, true);
        this.leftLeg = new Leg(false, false);
    }

    public void setHairColor(String hairColor) {
        this.head.setHairColor(hairColor);
    }

    public void setEyeColor(String eyeColor) {
        this.head.setEyeColor(eyeColor);
    }

    public Head getHead() {
        return head;
    }

    public Hand getRightHand() {
        return rightHand;
    }

    public Hand getLeftHand() {
        return leftHand;
    }

    public Leg getRightLeg() {
        return rightLeg;
    }

    public Leg getLeftLeg() {
        return leftLeg;
    }


    @Override
    public String toString() {
        return "Human{" +
                "head=" + head +
                ", rightHand=" + rightHand +
                ", leftHand=" + leftHand +
                ", rightLeg=" + rightLeg +
                ", leftLeg=" + leftLeg +
                '}';
    }
}

package forth;

public class Book {
    private int pageCount;
    private String author;
    private String title;

    private String atCreated;

    public Book(int pageCount, String author, String title, String atCreated) {
        this.pageCount = pageCount;
        this.author = author;
        this.title = title;
        this.atCreated = atCreated;
    }

    public Book(int pageCount, String title) {
        this.pageCount = pageCount;
        this.title = title;
        this.author = "Unknown";
        this.atCreated = "Unknown";
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAtCreated() {
        return atCreated;
    }

    public void setAtCreated(String atCreated) {
        this.atCreated = atCreated;
    }

    @Override
    public String toString() {
        return "Book{" +
                "pageCount=" + pageCount +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", atCreated='" + atCreated + '\'' +
                '}';
    }
}

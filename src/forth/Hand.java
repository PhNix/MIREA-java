package forth;

public class Hand {
    private boolean isBroken;
    private boolean isRight;
    private boolean isLeft;

    public Hand(boolean isBroken, boolean isRight) {
        this.isBroken = isBroken;
        this.isRight = isRight;
        this.isLeft = !isRight;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public void setBroken(boolean broken) {
        isBroken = broken;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        isRight = right;
        isLeft = !right;
    }

    public boolean isLeft() {
        return isLeft;
    }

    public void setLeft(boolean left) {
        isLeft = left;
        isRight = !left;
    }

    @Override
    public String toString() {
        return "Hand{" +
                "isBroken=" + isBroken +
                ", isRight=" + isRight +
                ", isLeft=" + isLeft +
                '}';
    }
}

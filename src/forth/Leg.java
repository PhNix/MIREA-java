package forth;

public class Leg {
    private boolean isBroken;
    private boolean isRight;
    private boolean isLeft;

    public Leg(boolean isBroken, boolean isRight) {
        this.isBroken = isBroken;
        this.isRight = isRight;
        this.isLeft = !isRight;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public void setBroken(boolean broken) {
        isBroken = broken;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        isRight = right;
        isLeft = !right;
    }

    public boolean isLeft() {
        return isLeft;
    }

    public void setLeft(boolean left) {
        isLeft = left;
        isRight = !left;
    }


    @Override
    public String toString() {
        return "Leg{" +
                "isBroken=" + isBroken +
                ", isRight=" + isRight +
                ", isLeft=" + isLeft +
                '}';
    }
}

package forth;

public class Circle {
    private double radius;
    private double square;
    private double diameter;
    private double length;

    public Circle(double radius) {
        this.radius = radius;
        this.diameter = radius * 2;
        this.length = 2 * Math.PI * radius;
        this.square = Math.PI * Math.pow(radius, 2);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
        this.diameter = radius * 2;
        this.length = 2 * Math.PI * radius;
        this.square = Math.PI * Math.pow(radius, 2);
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
        this.radius = Math.sqrt(square / Math.PI);
        this.diameter = radius * 2;
        this.length = 2 * Math.PI * radius;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
        this.radius = diameter / 2;
        this.length = 2 * Math.PI * radius;
        this.square = Math.PI * Math.pow(radius, 2);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
        this.radius = length / (2*Math.PI);
        this.diameter = radius * 2;
        this.square = Math.PI * Math.pow(radius, 2);
    }
}

package twentytwoth;

public interface ComplexAbstractFactory {
    Complex createComplex();

    Complex CreateComplex(double real, double imaginary);
}

package twentytwoth;

public class ComplexFactory implements ComplexAbstractFactory {
    private double real;
    private double imaginary;

    @Override
    public Complex createComplex() {
        return new Complex(real, imaginary);
    }

    @Override
    public Complex CreateComplex(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
        return createComplex();
    }

}

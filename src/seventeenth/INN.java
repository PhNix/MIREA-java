package seventeenth;

import java.util.Scanner;

public class INN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите ФИО: ");
        String fullName = scanner.nextLine();

        System.out.print("Введите ИНН: ");
        String inn = scanner.nextLine();

        try {
            if (isValidINN(inn)) {
                System.out.println("Ваш номер ИНН верен. Заказ оформлен на: " + fullName);
            } else {
                throw new Exception("Недействительный номер ИНН.");
            }
        } catch (Exception e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }
    public static boolean isValidINN(String inn) {
        if (inn.length() != 10 && inn.length() != 12) {
            return false;
        }

        try {
            long num = Long.parseLong(inn);
        } catch (NumberFormatException e) {
            return false;
        }

        if (inn.length() == 10) {
            return checkINN10Digits(inn);
        } else {
            return checkINN12Digits(inn);
        }
    }

    private static boolean checkINN10Digits(String inn) {
        // n = ((2n1 + 4n2 + 10n3 + 3n4 + 5n5 + 9n6 + 4n7 + 6n8 + 8n9) mod 11) mod 10
        int[] n = {2, 4, 10, 3, 5, 9, 4, 6, 8};
        int sum = 0;

        for (int i = 0; i < 9; i++) {
            sum += Character.getNumericValue(inn.charAt(i)) * n[i];
        }

        int mod = sum % 11;
        if (mod == 10) {
            mod = 0;
        }

        int result = Character.getNumericValue(inn.charAt(9));

        return mod == result;
    }

    private static boolean checkINN12Digits(String inn) {
        // n1 = ((7n2 + 2n1 + 4n2 + 10n3 + 3n4 + 5n5 + 9n6 + 4n7 + 6n8 + 8n9) mod 11) mod 10
        // n2 = ((3n1 + 7n2 + 2n1 + 4n2 + 10n3 + 3n4 + 5n5 + 9n6 + 4n7 + 6n8 + 8n9) mod 11) mod 10
        int[] n1 = {7, 2, 4, 10, 3, 5, 9, 4, 6, 8};
        int[] n2 = {3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8};
        int sum1 = 0;
        int sum2 = 0;

        for (int i = 0; i < 10; i++) {
            sum1 += Character.getNumericValue(inn.charAt(i)) * n1[i];
            sum2 += Character.getNumericValue(inn.charAt(i)) * n2[i];
        }

        int mod1 = sum1 % 11;
        if (mod1 == 10) {
            mod1 = 0;
        }

        int mod2 = sum2 % 11;
        if (mod2 == 10) {
            mod2 = 0;
        }

        int result1 = Character.getNumericValue(inn.charAt(10));
        int result2 = Character.getNumericValue(inn.charAt(11));

        return mod1 == result1 && mod2 == result2;
    }

}

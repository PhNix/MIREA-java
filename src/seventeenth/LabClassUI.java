package seventeenth;

import java.util.Scanner;

public class LabClassUI {
    private LabClass labClass;
    private Scanner scanner;

    public LabClassUI(int capacity) {
        labClass = new LabClass(capacity);
        scanner = new Scanner(System.in);
    }

    public void addStudent() {
        System.out.println("Введите идентификационный номер студента: ");
        String iDNumber = scanner.nextLine();
        System.out.println("Введите полное имя студента: ");
        String fullName = scanner.nextLine();

        try {
            if (iDNumber.isEmpty() || fullName.isEmpty()) {
                throw new EmptyStringException("Пустой идентификационный номер или полное имя.");
            }

            Student student = new Student(iDNumber, fullName);
            labClass.addStudent(student);
            System.out.println("Студент успешно добавлен.");
        } catch (EmptyStringException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void searchStudent() {
        System.out.println("Введите полное имя студента: ");
        String fullName = scanner.nextLine();

        try {
            if (fullName.isEmpty()) {
                throw new EmptyStringException("Пустое имя.");
            }

            Student[] students = labClass.getStudents();
            boolean found = false;

            for (int i = 0; i < labClass.getSize(); i++) {
                if (students[i].getFullName().equalsIgnoreCase(fullName)) {
                    System.out.println("Студент найден: " + students[i]);
                    found = true;
                    break;
                }
            }

            if (!found) {
                throw new StudentNotFoundException("Студент не найден.");
            }
        } catch (EmptyStringException | StudentNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void sortStudentsByIDNumber() {
        Student[] students = labClass.getStudents();
        int n = labClass.getSize();

        for (int i = 1; i < n; i++) {
            Student key = students[i];
            int j = i - 1;

            while (j >= 0 && students[j].getIDNumber().compareTo(key.getIDNumber()) > 0) {
                students[j + 1] = students[j];
                j = j - 1;
            }

            students[j + 1] = key;
        }

        System.out.println("Студенты отсортированы по идентификационному номеру.");
    }

    public void displayStudents() {
        Student[] students = labClass.getStudents();

        for (int i = 0; i < labClass.getSize(); i++) {
            System.out.println(students[i]);
        }
    }
}

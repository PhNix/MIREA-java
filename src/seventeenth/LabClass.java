package seventeenth;

public class LabClass {
    private Student[] students;
    private int size;

    public LabClass(int capacity) {
        students = new Student[capacity];
        size = 0;
    }

    public void addStudent(Student student) {
        students[size] = student;
        size++;
    }

    public Student[] getStudents() {
        return students;
    }

    public int getSize() {
        return size;
    }
}

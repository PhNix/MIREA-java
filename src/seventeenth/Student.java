package seventeenth;

public class Student {
    private String iDNumber;
    private String fullName;

    public Student(String iDNumber, String fullName) {
        this.iDNumber = iDNumber;
        this.fullName = fullName;
    }

    public String getIDNumber() {
        return iDNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setIDNumber(String iDNumber) {
        this.iDNumber = iDNumber;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "Студент {" +
                "ID: '" + iDNumber + '\'' +
                ", Полное имя: '" + fullName + '\'' +
                '}';
    }
}

package seventeenth;

import java.util.Scanner;

public class LabClassDriver {
    public static void main(String[] args) {
        LabClassUI labClassUI = new LabClassUI(5);

        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            System.out.println();
            System.out.println("1. Добавить студента");
            System.out.println("2. Поиск студента");
            System.out.println("3. Сортировка студентов по идентификационному номеру");
            System.out.println("4. Показать студентов");
            System.out.println("0. Выход");
            System.out.println("Введите свой выбор: ");

            choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            switch (choice) {
                case 1:
                    labClassUI.addStudent();
                    break;
                case 2:
                    labClassUI.searchStudent();
                    break;
                case 3:
                    labClassUI.sortStudentsByIDNumber();
                    break;
                case 4:
                    labClassUI.displayStudents();
                    break;
                case 0:
                    System.out.println("Завершение работы...");
                    break;
                default:
                    System.out.println("Неверный выбор. Пожалуйста, попробуйте еще раз.");
            }
        } while (choice != 0);
    }
}

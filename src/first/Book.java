package first;

public class Book {
    private int pageCount;
    private String author;
    private String title;

    public Book(int pageCount, String author, String title) {
        this.pageCount = pageCount;
        this.author = author;
        this.title = title;
    }

    public Book(int pageCount, String title) {
        this.pageCount = pageCount;
        this.title = title;
        this.author = "Unknown";
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "pageCount=" + pageCount +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}

package first;

public class Dog {
    private int age;
    private String breed;
    private String name;
    private boolean isWild;

    public Dog(int age, String breed, String name, boolean isWild) {
        this.age = age;
        this.breed = breed;
        this.name = name;
        this.isWild = isWild;
    }

    public Dog(int age, String breed, String name) {
        this.age = age;
        this.breed = breed;
        this.name = name;
        this.isWild = false;
    }

    public Dog(int age, String name) {
        this.age = age;
        this.name = name;
        this.breed = "Unknown";
        this.isWild = false;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsWild() {
        return isWild;
    }

    public void setIsWild(boolean isWild) {
        this.isWild = isWild;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "age=" + age +
                ", breed='" + breed + '\'' +
                ", name='" + name + '\'' +
                ", isWild=" + isWild +
                '}';
    }

    public void intoHumanAge() {
        System.out.println("Возраст " + name + " в человечиских годах " + age*7);
    }
}

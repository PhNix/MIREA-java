package first;

public class Ball {
    private String color;
    private int weight;

    private String type;


    public Ball(String color, int weight, String type) {
        this.color = color;
        this.weight = weight;
        this.type = type;
    }

    public Ball(String color, int weight) {
        this.color = color;
        this.weight = weight;
        this.type = "Unknown";
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Ball{" +
                "color='" + color + '\'' +
                ", weight=" + weight +
                ", type='" + type + '\'' +
                '}';
    }
}

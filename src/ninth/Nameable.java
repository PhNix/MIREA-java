package ninth;

public interface Nameable {
    String getName();
}

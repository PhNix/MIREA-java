package ninth;

public interface Priceable {
    double getPrice();
}

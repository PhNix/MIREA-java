package twentythreeth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InternetOrder {
    private InternetOrderItem head;
    private int size;
    public InternetOrder() {
        head = new InternetOrderItem(null);
        head.next = head;
        head.prev = head;
        size = 0;
    }

    InternetOrder(Item[] items) {
        this();
        addItems(items);
    }

    private void addItems(Item[] items) {
        for (Item item : items) {
            addItem(item);
        }
    }

    public void addItem(Item item) {
        if (item == null) {
            return;
        }

        InternetOrderItem newNode = new InternetOrderItem(item);
        newNode.next = head;
        newNode.prev = head.prev;
        head.prev.next = newNode;
        head.prev = newNode;
        size++;

    }

    public boolean removeItem(String itemName) {
        if (itemName == null) {
            return false;
        }

        InternetOrderItem current = head.prev;
        while (current != head) {
            if (current.item.getName().equals(itemName)) {
                current.prev.next = current.next;
                current.next.prev = current.prev;
                size--;
                return true;
            }
            current = current.prev;
        }

        return false;
    }

    public int removeAll(String itemName) {
        if (itemName == null) {
            return 0;
        }

        int count = 0;
        InternetOrderItem current = head.prev;
        while (current != head) {
            if (current.item.getName().equals(itemName)) {
                current.prev.next = current.next;
                current.next.prev = current.prev;
                count++;
                size--;
            }
            current = current.prev;
        }

        return count;
    }

    public int getTotalItemCount() {
        return size;
    }

    public Item[] getItems() {
        Item[] items = new Item[size];
        InternetOrderItem current = head.next;
        int index = 0;
        while (current != head) {
            items[index] = current.item;
            index++;
            current = current.next;
        }
        return items;
    }

    public double getTotalCost() {
        double totalCost = 0.0;
        InternetOrderItem current = head.next;
        while (current != head) {
            totalCost += current.item.getCost();
            current = current.next;
        }
        return totalCost;
    }

    public int getItemCount(String itemName) {
        if (itemName == null) {
            return 0;
        }

        int count = 0;
        InternetOrderItem current = head.prev;
        while (current != head) {
            if (current.item.getName().equals(itemName)) {
                count++;
            }
            current = current.prev;
        }

        return count;
    }

    public String[] getItemNames() {
        List<String> uniqueNames = new ArrayList<>();
        InternetOrderItem current = head.next;
        while (current != head) {
            String itemName = current.item.getName();
            if (!uniqueNames.contains(itemName)) {
                uniqueNames.add(itemName);
            }
            current = current.next;
        }
        return uniqueNames.toArray(new String[0]);
    }

    public Item[] getSortedItemsByPrice() {
        Item[] items = getItems();
        Arrays.sort(items, (item1, item2) -> Double.compare(item2.getCost(), item1.getCost()));
        return items;
    }

}

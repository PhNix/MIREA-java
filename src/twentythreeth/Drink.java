package twentythreeth;

final public class Drink implements Item {
    private static final double DEFAULT_COST = 0.0;
    private static final String DEFAULT_NAME = "";
    private static final String DEFAULT_DESCRIPTION = "";

    private final double cost;
    private final String name;
    private final String description;

    Drink(String name, String description) {
        this(DEFAULT_COST, name, description);
    }

    Drink(double cost, String name, String description) {
        this.cost = cost;
        this.name = name;
        this.description = description;
    }

    public double getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }


}

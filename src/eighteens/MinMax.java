package eighteens;

public class MinMax<T extends Comparable<T>> {
    private T[] arr;

    public MinMax(T[] array) {
        this.arr = array;
    }

    public T findMin() {
        T min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i].compareTo(min) < 0) {
                min = arr[i];
            }
        }
        return min;
    }

    public T findMax() {
        T max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i].compareTo(max) > 0) {
                max = arr[i];
            }
        }
        return max;
    }
}

package eighteens;

public class Calculator {
    public static <T, U> double sum(T t, U u) {
        double n1 = Double.parseDouble(t.toString());
        double n2 = Double.parseDouble(u.toString());
        return n1 + n2;
    }

    public static <T, U> double multiply(T t, U u) {
        double n1 = Double.parseDouble(t.toString());
        double n2 = Double.parseDouble(u.toString());
        return n1 * n2;
    }

    public static <T, U> double divide(T t, U u) {
        double n1 = Double.parseDouble(t.toString());
        double n2 = Double.parseDouble(u.toString());
        return n1 / n2;
    }

    public static <T, U> double subtraction(T t, U u) {
        double n1 = Double.parseDouble(t.toString());
        double n2 = Double.parseDouble(u.toString());
        return n1 - n2;
    }
}

package eighteens;

import java.io.Serializable;

public class Default<T extends Comparable, V extends Serializable & Animal, K>{
    private T t;
    private V v;
    private K k;

    public Default(T t, V v, K k) {
        this.t = t;
        this.v = v;
        this.k = k;
    }

    public T getT() {
        return t;
    }

    public V getV() {
        return v;
    }

    public K getK() {
        return k;
    }

    public void printClassNames() {
        System.out.println("T(class): " + t.getClass().getName());
        System.out.println("V(class): " + v.getClass().getName());
        System.out.println("K(class): " + k.getClass().getName());
    }

}

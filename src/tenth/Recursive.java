package tenth;

import java.util.Scanner;

public class Recursive {

    public static void oddNumbers() {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        if (number != 0) {
            if (number % 2 != 0) {
                System.out.println(number);
            }
            oddNumbers();
        }
    }

    public static void oddPositionNumbers() {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        if (number != 0) {
            System.out.println(number);
            int n = scanner.nextInt();
            if (n == 0) return;
            oddPositionNumbers();
        }
    }

    public static void leftToRight(int number) {
        if (number >= 10) {
            leftToRight(number / 10);
        }

        System.out.print(number % 10 + " ");
    }

    public static void rightToLeft(int number) {
        if (number >= 10) {
            System.out.print(number % 10 + " ");
            rightToLeft(number / 10);
        } else {
            System.out.print(number + " ");
        }
    }

}

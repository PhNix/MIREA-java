package twentyth;

import java.util.Stack;

public class Model {
    private Stack<Double> numbers;
    private Stack<String> operators;

    public Model(){
        numbers = new Stack<>();
        operators = new Stack<>();
    }

    public void reset(){
        numbers.clear();
        operators.clear();
    }

    public double calculateResult(String input) {
        String[] tokens = input.split(" ");

        for (String token : tokens) {
            if (isNumber(token)) {
                numbers.push(Double.parseDouble(token));
            } else {
                operators.push(token);
                resolve();
            }
        }

        while(!operators.isEmpty()){
            resolve();
        }

        if(numbers.size() != 1){
            throw new IllegalArgumentException("Неверное выражение");
        }
        return numbers.pop();
    }

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
            return true;
        } catch(NumberFormatException e) {
            return false;
        }
    }

    private void resolve() {
        if (operators.isEmpty() || numbers.size() < 2)
            return;

        String operator = operators.pop();
        double operand2 = numbers.pop();
        double operand1 = numbers.pop();

        switch (operator) {
            case "+":
                numbers.push(operand1 + operand2);
                break;
            case "-":
                numbers.push(operand1 - operand2);
                break;
            case "*":
                numbers.push(operand1 * operand2);
                break;
            case "/":
                if (operand2 == 0)
                    throw new IllegalArgumentException("Нельзя делить на ноль");
                numbers.push(operand1 / operand2);
                break;
            default:
                throw new IllegalArgumentException("Неверный оператор");
        }
    }
}

package twentyfourth;

import twentythreeth.Item;

public interface Order {
    boolean addItem(Item item);
    boolean removeItem(String itemName);
    int removeAll(String itemName);
    int getTotalCount();
    Item[] getItems();
    double getTotalPrice();
    int getCount(String itemName);
    String[] getOrderedItems();
    Item[] getSortedItems();
}

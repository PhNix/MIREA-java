package eleventh;

import java.util.Comparator;

public class SortingStudentsByGPA implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return Double.compare(o1.getGpa(), o2.getGpa());
    }

    private static void quickSort(Student[] students, int left, int right) {
        if (left < right) {
            int part = getPartition(students, left, right);

            quickSort(students, left, part - 1);
            quickSort(students, part + 1, right);
        }
    }

    private static int getPartition(Student[] students, int left, int right) {
        double midPivot = students[right].getGpa();
        int i = (left - 1);

        for (int j = left; j < right; j++) {
            if (students[j].getGpa() >= midPivot) {
                i++;
                Student temp = students[i];
                students[i] = students[j];
                students[j] = temp;
            }
        }

        Student temp = students[i + 1];
        students[i + 1] = students[right];
        students[right] = temp;

        return i + 1;
    }

    public static void main(String[] args) {
        Student[] students = {
                new Student("Arthur", 3.8),
                new Student("Bob", 3.5),
                new Student("Steph", 4.0),
                new Student("Max", 3.2),
                new Student("Kat", 3.9)
        };

        quickSort(students, 0, students.length - 1);

        for (Student student: students) {
            System.out.println("Имя: " + student.getName() + ", GPA: " + student.getGpa());
        }
    }
}

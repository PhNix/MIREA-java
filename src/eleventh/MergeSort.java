package eleventh;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MergeSort {
    public static List<Student> mergeSort(List<Student> list1, List<Student> list2) {
        List<Student> mergedList = new ArrayList<>();
        int i = 0, j = 0;

        while (i < list1.size() && j < list2.size()) {
            if (list1.get(i).getGpa() < list2.get(j).getGpa()) {
                mergedList.add(list1.get(i));
                i++;
            } else {
                mergedList.add(list2.get(j));
                j++;
            }
        }

        while (i < list1.size()) {
            mergedList.add(list1.get(i));
            i++;
        }

        while (j < list2.size()) {
            mergedList.add(list2.get(j));
            j++;
        }

        mergedList.sort(Comparator.comparingDouble(Student::getGpa));
        return mergedList;
    }

    public static void main(String[] args) {
        List<Student> list1 = new ArrayList<>();
        list1.add(new Student("Arthur", 3.8));
        list1.add(new Student("Bob", 3.5));
        list1.add(new Student("Steph", 4.0));

        List<Student> list2 = new ArrayList<>();
        list2.add(new Student("Max", 3.2));
        list2.add(new Student("Kat", 3.9));

        List<Student> mergedList = mergeSort(list1, list2);

        for (Student student : mergedList) {
            System.out.println("Имя: " + student.getName() + ", GPA: " + student.getGpa());
        }
    }
}

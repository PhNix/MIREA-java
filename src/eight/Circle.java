package eight;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Circle extends Shape{
    private int radius;

    public Circle(Color color, int x, int y, int radius) {
        super(color, x, y);
        this.radius = radius;
    }

    @Override
    void draw(Graphics2D graphics) {
        Ellipse2D.Double circle = new Ellipse2D.Double(x, y, radius, radius);
        graphics.setColor(color);
        graphics.fill(circle);
    }
}


package eight;

import javax.swing.*;
import java.awt.*;

public class ShapeWindow extends JPanel {
    private final int WINDOW_WIDTH = 680;
    private final int WINDOW_HEIGHT = 680;

    private Shape[] shapes;

    public ShapeWindow(Shape[] shapes) {
        this.shapes = shapes;
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics = (Graphics2D) g;

        for (Shape shape : shapes) {
            shape.draw(graphics);
        }
    }
}

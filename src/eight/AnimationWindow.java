package eight;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

public class AnimationWindow extends JPanel {
    private  Timer timer;
    private BufferedImage[] frames;
    private int frameIndex;
    private final int WINDOW_WIDTH = 680;
    private final int WINDOW_HEIGHT = 680;

    public AnimationWindow(String[] framePaths, int delay) {
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        frames = new BufferedImage[framePaths.length];
        for (int i = 0; i < framePaths.length; i++) {
            try {
                frames[i] = ImageIO.read(new File(framePaths[i]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        timer = new Timer(delay, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frameIndex = (frameIndex + 1) % frames.length;
                repaint();
            }
        });
        timer.start();
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(frames[frameIndex], 0, 0, null);
    }
}

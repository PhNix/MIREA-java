package eight;

import javax.swing.*;
import java.awt.*;

public class Application {
    public static void main(String[] args) {
        // Окно с 20 рандомными фигурами
        JFrame shapesFrame = new JFrame("Shape");
        shapesFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Shape[] shapes = new Shape[20];
        for (int i = 0; i < 20; i++) {
            int x = (int) (Math.random() * 380);
            int y = (int) (Math.random() * 380);
            int size = (int) (Math.random() * 50) + 10;
            int red = (int) (Math.random() * 256);
            int green = (int) (Math.random() * 256);
            int blue = (int) (Math.random() * 256);

            Color color = new Color(red, green, blue);

            if (i % 2 == 0) {
                shapes[i] = new Circle(color, x, y, size);
            } else {
                shapes[i] = new Rectangle(color, x, y, size, size);
            }
        }

        ShapeWindow shapesWindow = new ShapeWindow(shapes);
        shapesFrame.getContentPane().add(shapesWindow);
        shapesFrame.pack();
        shapesFrame.setVisible(true);

        // Окно с изображением

        JFrame imageFrame = new JFrame("Image");
        imageFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String imagePath = args[0];
        ImageWindow imageWindow = new ImageWindow(imagePath);
        imageFrame.getContentPane().add(imageWindow);
        imageFrame.pack();
        imageFrame.setVisible(true);

        // Окно с анимацией
        JFrame animationFrame = new JFrame("Animation");
        animationFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String[] framePaths = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            framePaths[i] = args[i];
        }
        int delay = 1000; // задержка в миллисекундах между кадрами
        AnimationWindow animationWindow = new AnimationWindow(framePaths, delay);
        animationFrame.getContentPane().add(animationWindow);
        animationFrame.pack();
        animationFrame.setVisible(true);
    }
}

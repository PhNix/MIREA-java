package eight;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Rectangle extends Shape {
    private int width;
    private int height;

    public Rectangle(Color color, int x, int y, int width, int height) {
        super(color, x, y);
        this.width = width;
        this.height = height;
    }

    @Override
    void draw(Graphics2D graphics) {
        Rectangle2D.Double rect = new Rectangle2D.Double(x, y, width, height);
        graphics.setColor(color);
        graphics.fill(rect);
    }
}

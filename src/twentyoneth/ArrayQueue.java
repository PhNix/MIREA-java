package twentyoneth;

// Один Экземпляр Очереди в виде класса
// неявная ссылка на экземлпяр очереди
// Циклическая
public class ArrayQueue {
    private Object[] nodes;
    private int size;
    private int head;
    private int tail;

    public ArrayQueue() {
        nodes = new Object[10];
        size = 0;
        head = 0;
        tail = 0;
    }

    public void enqueue(Object element) {
        assert element != null : "Element cannot be null"; // Before: element != null

        ensureCapacity(size + 1);
        nodes[tail] = element;
        tail = (tail + 1) % nodes.length;
        size++;
    }

    public Object element() {
        assert size > 0 : "Queue is empty";     // Before: очередь не пуста
        return nodes[head];     // After: возвращается первый элемент очереди

    }



    public Object dequeue() {
        assert size > 0 : "Queue is empty"; // Before: очередь не пуста
        Object element = nodes[head];
        nodes[head] = null;
        head = (head + 1) % nodes.length;
        size--;
        return element; // After: из очереди удален и возвращен первый элемент
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            nodes[(head + i) % nodes.length] = null;
        }
        head = 0;
        tail = 0;
        size = 0;
    }

    private void ensureCapacity(int capacity) {
        if (capacity > nodes.length) {
            Object[] newNodes = new Object[2 * capacity];
            System.arraycopy(nodes, head, newNodes, 0, nodes.length - head);
            System.arraycopy(nodes, 0, newNodes, nodes.length - head, tail);
            nodes = newNodes;
            head = 0;
            tail = size;
        }
    }
}

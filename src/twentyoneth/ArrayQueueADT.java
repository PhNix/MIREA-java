package twentyoneth;

// Один Экземпляр Очереди в виде абстрактного типа данных
// явная ссылка на экземлпяр очереди
// Циклическая
public class ArrayQueueADT {
    private Object[] nodes = new Object[10];
    private int size;
    private int head;
    private int tail;


    public static void enqueue(ArrayQueueADT queue, Object element) {
        assert element != null : "Element cannot be null";  // Before: element != null
        ensureCapacity(queue, queue.size + 1);
        queue.nodes[queue.tail] = element;
        queue.tail = (queue.tail + 1) % queue.nodes.length;
        queue.size++;
    }



    public static Object element(ArrayQueueADT queue) {
        assert queue.size > 0 : "Queue is empty"; // Before: очередь не пуста
        return queue.nodes[queue.head]; // After: возвращается первый элемент очереди
    }



    public static Object dequeue(ArrayQueueADT queue) {
        assert queue.size > 0 : "Queue is empty"; // Before: очередь не пуста
        Object element = queue.nodes[queue.head];
        queue.nodes[queue.head] = null;
        queue.head = (queue.head + 1) % queue.nodes.length;
        queue.size--;
        return element; // After: из очереди удален и возвращен первый элемент
    }

    public static int size(ArrayQueueADT queue) {
        return queue.size;
    }

    public static boolean isEmpty(ArrayQueueADT queue) {
        return queue.size == 0;
    }

    public static void clear(ArrayQueueADT queue) {
        for (int i = 0; i < queue.size; i++) {
            queue.nodes[(queue.head + i) % queue.nodes.length] = null;
        }
        queue.head = 0;
        queue.tail = 0;
        queue.size = 0;
    }

    private static void ensureCapacity(ArrayQueueADT queue, int capacity) {
        if (capacity > queue.nodes.length) {
            Object[] newnodes = new Object[2 * capacity];
            System.arraycopy(queue.nodes, queue.head, newnodes, 0, queue.nodes.length - queue.head);
            System.arraycopy(queue.nodes, 0, newnodes, queue.nodes.length - queue.head, queue.tail);
            queue.nodes = newnodes;
            queue.head = 0;
            queue.tail = queue.size;
        }
    }

}

package twentyoneth;

// Один Экземпляр Очереди с использованием переменных очереди
// Циклическая
public class ArrayQueueModule {
    private static Object[] nodes = new Object[10];
    private static int size;
    private static int head;
    private static int tail;


    public static void enqueue(Object element) {
        assert element != null : "Element cannot be null"; // Before: element != null
        ensureCapacity(size + 1);
        nodes[tail] = element;
        tail = (tail + 1) % nodes.length;
        size++;
    }

    public static Object element() {
        assert size > 0 : "Queue is empty"; // Before: очередь не пуста
        return nodes[head]; // After: возвращается первый элемент очереди
    }



    public static Object dequeue() {
        assert size > 0 : "Queue is empty"; // Before: очередь не пуста
        Object element = nodes[head];
        nodes[head] = null;
        head = (head + 1) % nodes.length;
        size--;
        return element;  // After: из очереди удален и возвращен первый элемент
    }
    public static int size() {
        return size;
    }

    public static boolean isEmpty() {
        return size == 0;
    }
    public static void clear() {
        for (int i = 0; i < size; i++) {
            nodes[(head + i) % nodes.length] = null;
        }
        head = 0;
        tail = 0;
        size = 0;
    }

    private static void ensureCapacity(int capacity) {
        if (capacity > nodes.length) {
            Object[] newElements = new Object[2 * capacity];
            System.arraycopy(nodes, head, newElements, 0, nodes.length - head);
            System.arraycopy(nodes, 0, newElements, nodes.length - head, tail);
            nodes = newElements;
            head = 0;
            tail = size;
        }
    }
}

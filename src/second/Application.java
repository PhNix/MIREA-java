package second;

import java.util.Arrays;
import java.util.Random;

public class Application {

    public static void main(String[] args) {
        // Task 1
        sumWithFor();
        sumWithWhile();
        sumWithDoWhile();

        // Task 2
        for (String arg : args) {
            System.out.println(arg);
        }

        // Task 3
        harmonicSeries();

        // Task 4
        randomArray();

        // Task 5
        factorial(3);
        factorial(7);
    }

    private static void sumWithFor() {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9};
        // int[] array = new int[]{};
        int sum = 0;
        for (int j : array) {
            sum += j;
        }
        System.out.println("Сумма чисел массива через цикл for: " + sum);
    }

    private static void sumWithWhile() {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9};
        // int[] array = new int[]{};
        int sum = 0;
        int i = 0;
        while(i < array.length) {
            sum += array[i];
            i++;
        }
        System.out.println("Сумма чисел массива через цикл while: " + sum);
    }

    private static void sumWithDoWhile() {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9};
        // int[] array = new int[]{};
        int sum = 0;
        int i = 0;
        do {
            if (array.length != 0) {
                sum += array[i];
            }
            i++;
        } while (i < array.length);
        System.out.println("Сумма чисел массива через цикл do-while: " + sum);
    }

    private static void harmonicSeries() {
        int num = 1;
        System.out.println("Гармонический ряд");
        for (int i = 1; i <= 10; i++) {
            System.out.println("1/" + (double) num);
            num++;
        }

    }

    private static void randomArray() {
        int[] randomArray = new int[10];
        Random rnd = new Random();
        for (int i = 0; i < randomArray.length; i++) {
            randomArray[i] = rnd.nextInt(100);
        }

        System.out.println(Arrays.toString(randomArray));
        bubbleSort(randomArray);
        System.out.println(Arrays.toString(randomArray));
    }

    private static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    private static void factorial(int number) {
        int res = 1;
        for (int i = 1; i <= number; i++) {
            res *= i;
        }
        System.out.println("Факториал числа(" + number + "): " + res);
    }
}

package forteenth;

public class Main {
    public static void main(String[] args) {
        IWaitList<Integer> waitList = new WaitList<>();
        waitList.add(1);
        waitList.add(2);
        waitList.add(3);
        System.out.println(waitList);
        waitList.remove();
        System.out.println(waitList);
        System.out.println(waitList.contains(2));
        System.out.println(waitList.isEmpty());

        BoundedWaitList<Integer> boundedWaitList = new BoundedWaitList<>(5);
        for (int i = 0; i < 10; i++) {
            boundedWaitList.add(i);
        }
        System.out.println(boundedWaitList);

        UnfairWaitList<String> unfairWaitList = new UnfairWaitList<>();
        unfairWaitList.add("A");
        unfairWaitList.add("B");
        unfairWaitList.add("C");
        System.out.println(unfairWaitList);
        unfairWaitList.moveToBack("A");
        System.out.println(unfairWaitList);
        unfairWaitList.remove("C");
        System.out.println(unfairWaitList);
    }
}

package sixth;

abstract class Furniture {
    private String color;
    private double weight;
    private String material;

    public Furniture(String color, double weight, String material) {
        this.color = color;
        this.weight = weight;
        this.material = material;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public String getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Furniture{" +
                "color='" + color + '\'' +
                ", weight=" + weight +
                ", material='" + material + '\'' +
                '}';
    }
}

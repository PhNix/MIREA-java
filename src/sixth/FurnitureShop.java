package sixth;

import java.util.ArrayList;
import java.util.List;

public class FurnitureShop {
    private List<Furniture> furnitures = new ArrayList<>();

    public void buyChair(Chair chair) {
        furnitures.add(chair);
    }

    public void buyTable(Table table) {
        furnitures.add(table);
    }
    List<Furniture> getFurniture() {
        return furnitures;
    }
}

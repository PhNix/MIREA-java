package sixth;

abstract  class Dish {
    private String name;
    private double weight;
    private String material;

    public Dish(String name, double weight, String material) {
        this.name = name;
        this.weight = weight;
        this.material = material;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public String getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", material='" + material + '\'' +
                '}';
    }
}

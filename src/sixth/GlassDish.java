package sixth;

public class GlassDish extends Dish{
    GlassDish(String name, double weight) {
        super(name, weight, "glass");
    }
}

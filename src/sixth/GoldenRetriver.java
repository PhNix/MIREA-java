package sixth;

public class GoldenRetriver extends Dog{
    public GoldenRetriver(int age, String name, boolean isWild) {
        super(age, "Золотой ретривер", name, isWild);
    }

    public GoldenRetriver(int age, String name) {
        super(age, "Золотой ретривер", name);
    }

}

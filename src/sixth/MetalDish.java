package sixth;

public class MetalDish extends Dish{
    MetalDish(String name, double weight) {
        super(name, weight, "metal");
    }
}

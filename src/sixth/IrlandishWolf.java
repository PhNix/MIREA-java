package sixth;

public class IrlandishWolf extends Dog{

    private boolean isHunt;

    public IrlandishWolf(int age, String name, boolean isWild, boolean isHunt) {
        super(age, "Ирландский волкодав", name, isWild);
        this.isHunt = isHunt;
    }

    public IrlandishWolf(int age, String name, boolean isHunt) {
        super(age, "Ирландский волкодав", name);
        this.isHunt = isHunt;
    }
}

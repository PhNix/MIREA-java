package thirteenth;

import java.io.*;
import java.util.Scanner;

public class FileManipulations {

    private static void writeToTextFile(String fileName, String text) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(text);
        writer.close();
    }

    private static String readFromTextFile(String fileName) throws IOException {
        StringBuilder fileContent = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;
        while ((line = reader.readLine()) != null)
        {
            fileContent.append(line).append("\n");
        }
        return fileContent.toString();
    }


    private static void replaceTextInFile(String fileName, String oldText, String newText) throws IOException {
        String fileContent = readFromTextFile(fileName);
        fileContent = fileContent.replace(oldText, newText);
        writeToTextFile(fileName, fileContent);
    }

    private static void appendTextToFile(String fileName, String text) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write(text);
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите текст для записи в файл:");
        String textToWrite = scanner.nextLine();
        writeToTextFile("sample.txt", textToWrite);

        // 2. Вывод информации из файла на экран
        System.out.println("Содержимое файла sample.txt:");
        String fileContent = readFromTextFile("sample.txt");
        System.out.println(fileContent);

        // 3. Замена информации в файле
        System.out.println("Введите текст для замены в файле:");
        String oldText = scanner.nextLine();
        System.out.println("Введите текст на который надо заменить:");
        String newText = scanner.nextLine();
        replaceTextInFile("sample.txt", oldText, newText);

        // 4. Добавление текста в конец файла
        System.out.println("Введите текст для добавления в конец файла:");
        String textToAdd = scanner.nextLine();
        appendTextToFile("sample.txt", textToAdd);

    }
}

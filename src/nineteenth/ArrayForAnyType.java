package nineteenth;

public class ArrayForAnyType<T> {
    private T[] arr;

    public ArrayForAnyType(T[] arr) {
        this.arr = arr;
    }

    public T getElement(int index) {
        return arr[index];
    }

    public static void main(String[] args) {
        Integer[] intArray = {1, 2, 3, 4, 5};
        ArrayForAnyType<Integer> arrayHolder = new ArrayForAnyType<>(intArray);
        System.out.println(arrayHolder.getElement(2));

        String[] stringArray = {"Hello", "World"};
        ArrayForAnyType<String> stringArrayHolder = new ArrayForAnyType<>(stringArray);
        System.out.println(stringArrayHolder.getElement(1));
    }
}

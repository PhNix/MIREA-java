package nineteenth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FromArrayToList {
    public static <T> List<T> arrayToList(T[] array) {
        return new ArrayList<>(Arrays.asList(array));
    }

    public static void main(String[] args) {
        String[] stringArray = {"Hello", "World"};
        List<String> stringList = arrayToList(stringArray);
        System.out.println(stringList);

        Integer[] intArray = {2, 3, 0, 9, 4};
        List<Integer> intList = arrayToList(intArray);
        System.out.println(intList);
    }
}

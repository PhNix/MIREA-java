package third;

import third.Ball;

public class TestBall {
    public static void main(String[] args) {
        Ball b1 = new Ball(10, 10);
        System.out.println(b1.getX());
        System.out.println(b1.getY());

        b1.setXY(100, 20);
        System.out.println(b1);

        b1.move(10, -10);
        System.out.println(b1);
    }
}

package third;

import third.Author;

public class TestAuthor {

    public static void main(String[] args) {
        Author a1 = new Author("Рик Риордан", "rickriordan@gmail.com", 'M');

        System.out.println(a1.getName());
        System.out.println(a1.getEmail());
        System.out.println(a1.getGender());

        a1.setEmail("rickriordan@mail.ru");

        System.out.println(a1);
    }
}

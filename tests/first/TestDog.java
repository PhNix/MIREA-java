package first;

public class TestDog {
    public static void main(String[] args) {
        Dog d1 = new Dog(6, "Питбуль", "Мик", false);
        Dog d2 = new Dog(8, "Овчарка", "Дог", true);
        Dog d3 = new Dog(7, "Золотой ретривер", "Майк");
        Dog d4 = new Dog(5, "Ирландский волкодав", "Митчел");

        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);
        System.out.println(d4);

        d2.intoHumanAge();
        d4.intoHumanAge();

    }
}

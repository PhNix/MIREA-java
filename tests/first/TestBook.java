package first;

public class TestBook {
    public static void main(String[] args) {
        Book bk1 = new Book(456, "Рик Риордан", "Перси Джексон");
        Book bk2 = new Book(3434, "Герои Олимпа");

        System.out.println(bk1);
        System.out.println(bk2);
    }
}

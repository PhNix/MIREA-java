package eighteens;

public class TestGeneric {
    public static void main(String[] args) {
        Default<Integer, Frog, String> myClass = new Default<>(5, new Frog(), "test");
        myClass.printClassNames();
        System.out.println("T: " + myClass.getT());
        System.out.println("V: " + myClass.getV());
        System.out.println("K: " + myClass.getK());

        System.out.println("Сумма: " + Calculator.sum(5, 12));
        System.out.println("Умножение: " + Calculator.multiply(2, 25));
        System.out.println("Деление: " + Calculator.divide(16, 2));
        System.out.println("Вычитание: " + Calculator.subtraction(8.7, 3.7));

        Integer[] integers = {5, 2, 9, 1, 7};
        MinMax<Integer> minMax = new MinMax<>(integers);
        System.out.println("Минимальный элемент: " + minMax.findMin());
        System.out.println("Максимальный элемент: " + minMax.findMax());


        Integer[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        Matrix<Integer> integerMatrix = new Matrix<>(matrix);
        integerMatrix.printMatrix();
        System.out.println("Элемент в первой строке, второй столбец: " + integerMatrix.getElement(1, 2));
        integerMatrix.setElement(0, 0, 10);
        integerMatrix.printMatrix();
    }
}

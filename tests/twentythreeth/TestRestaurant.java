package twentythreeth;

public class TestRestaurant {
    public static void main(String[] args) {
        Drink coffee = new Drink(2.0, "Coffee", "Hot beverage made from roasted coffee beans");
        Drink tea = new Drink(1.5, "Tea", "Hot or cold infused beverage made from the Camellia sinensis leaves");
        Dish pizza = new Dish(8.5, "Pizza", "Flatbread topped with various ingredients");
        Dish salad = new Dish(6.0, "Salad", "Mixture of vegetables, often with added toppings and dressings");

        InternetOrder order = new InternetOrder(new Item[] {coffee, tea, pizza, salad});

        System.out.println("Total items in order: " + order.getTotalItemCount());
        System.out.println("Total cost of order: $" + order.getTotalCost());

        System.out.println("Item count of Coffee: " + order.getItemCount("Coffee"));
        System.out.println("Item count of Tea: " + order.getItemCount("Tea"));

        System.out.println("Item names in order:");
        String[] itemNames = order.getItemNames();
        for (String itemName : itemNames) {
            System.out.println(itemName);
        }

        System.out.println("Sorted items by price:");
        Item[] sortedItems = order.getSortedItemsByPrice();
        for (Item item : sortedItems) {
            System.out.println(item.getName() + " - $" + item.getCost());
        }
    }
}

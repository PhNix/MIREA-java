package eleventh;

public class TestSortByiDNumbers {
    public static void main(String[] args) {
        Student[] students = {
                new Student(3),
                new Student(1),
                new Student(2),
                new Student(5),
                new Student(4)
        };

        sortAlgorithm(students);

        for (Student student : students) {
            System.out.println("ID: " + student.getiDNumber());
        }
    }

    private static void sortAlgorithm(Student[] students) {
        int l = students.length;
        for (int i = 0; i < l; i++) {
            Student key = students[i];
            int j = i - 1;

            while (j >= 0 && students[j].getiDNumber() > key.getiDNumber()) {
                students[j + 1] = students[j];
                j = j - 1;
            }
            students[j + 1] = key;
        }
    }
}

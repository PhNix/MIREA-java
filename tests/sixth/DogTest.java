package sixth;


public class DogTest {

    public static void main(String[] args) {
        Dog d1 = new IrlandishWolf(5, "Митчел", true);
        d1.intoHumanAge();
        System.out.println(d1);

        Dog d2 = new GoldenRetriver(3, "Goodboy");
        d2.intoHumanAge();
        System.out.println(d2);
    }
}

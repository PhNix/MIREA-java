package sixth;

import java.util.List;

public class FurnitureTest {


    public static void main(String[] args) {
        FurnitureShop furnitureShop = new FurnitureShop();
        Chair f1 = new Chair("brown", 34.34, "wood");
        List<Furniture> furnitures = furnitureShop.getFurniture();
        furnitureShop.buyChair(f1);
        f1.setColor("red");

        System.out.println(f1);

        Furniture f2 = new Table("brown", 34.34, "wood");
        furnitureShop.buyTable((Table) f2);
        System.out.println(furnitureShop.getFurniture());

        System.out.println(f2);
    }
}

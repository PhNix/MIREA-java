package sixth;

public class DishTest {
    public static void main(String[] args) {
        Dish d1 = new GlassDish("plate", 1.34);
        System.out.println(d1);
        Dish d2 = new MetalDish("jug", 3.25);
        System.out.println(d2);

        System.out.println(d2.getMaterial());
        System.out.println(d2.getName());
        System.out.println(d2.getWeight());
    }
}

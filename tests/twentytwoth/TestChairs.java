package twentytwoth;

public class TestChairs {
    public static void main(String[] args) {
        // Создаем экземпляр фабрики
        AbstractChairFactory factory = new ChairFactory();

        // Создаем различные типы стульев с помощью фабрики
        VictorianChair victorianChair = factory.createVictorianChair();
        MagicChair magicChair = factory.createMagicChair();
        FunctionalChair functionalChair = factory.createFunctionalChair();

        // Создаем клиента
        Client client = new Client();

        // Устанавливаем стулья клиенту
        client.setChair(victorianChair);
        client.sit();

        client.setChair(magicChair);
        client.sit();

        client.setChair(functionalChair);
        client.sit();
    }
}

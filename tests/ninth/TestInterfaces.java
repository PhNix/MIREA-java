package ninth;

public class TestInterfaces {
    public static void main(String[] args) {
        Car c1 = new Car(2000);
        System.out.println(c1.getPrice());

        Planet p1 = new Planet("Earth");
        System.out.println(p1.getName());
    }
}

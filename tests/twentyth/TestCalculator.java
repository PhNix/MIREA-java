package twentyth;

public class TestCalculator {
    public static void main(String[] args) {
        String result;

        // Test 1: Normal operation
        result = ReversePolishNotation.calculate(new String[]{"2", "3", "+"});
        System.out.println("Ожидаемые результат: '5.0', Полученный результат: '" + result + "'");

        // Test 2: Invalid operator
        result = ReversePolishNotation.calculate(new String[]{"2", "3", "x"});
        System.out.println("Ожидаемые результат: 'Invalid operator: x', Полученный результат: '" + result + "'");

        // Test 3: Less operands
        result = ReversePolishNotation.calculate(new String[]{"2", "+"});
        System.out.println("Ожидаемые результат: 'Not enough operands for the operation: +', Полученный результат: '" + result + "'");

        // Test 4: More operands
        result = ReversePolishNotation.calculate(new String[]{"2", "3", "4", "+"});
        System.out.println("Ожидаемые результат: 'Invalid expression: more operands than operations', Полученный результат: '" + result + "'");

        // Test 5: Division by zero
        result = ReversePolishNotation.calculate(new String[]{"2", "0", "/"});
        System.out.println("Ожидаемые результат: 'Can't divide by zero', Полученный результат: '" + result + "'");

        // Test 6: Double overflow
        result = ReversePolishNotation.calculate(new String[]{Double.toString(Double.MAX_VALUE), "2", "*"});
        System.out.println("Ожидаемые результат: 'Number out of range: " + Double.POSITIVE_INFINITY + "', Полученный результат: '" + result + "'");

    }
}

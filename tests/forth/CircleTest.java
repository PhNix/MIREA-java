package forth;

public class CircleTest {
    public static void main(String[] args) {
        Circle c1 = new Circle(10);

        System.out.println(c1.getRadius());
        System.out.println(c1.getDiameter());
        System.out.println(c1.getLength());
        System.out.println(c1.getSquare());

        c1.setDiameter(40);

        System.out.println("----------------------");
        System.out.println(c1.getRadius());
        System.out.println(c1.getDiameter());
        System.out.println(c1.getLength());
        System.out.println(c1.getSquare());
    }
}

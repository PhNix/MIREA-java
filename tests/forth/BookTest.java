package forth;


public class BookTest {

    public static void main(String[] args) {
        Book bk1 = new Book(456, "Рик Риордан", "Перси Джексон", "2005");
        Book bk2 = new Book(3434, "Герои Олимпа");

        System.out.println(bk1);
        System.out.println(bk2);
    }
}

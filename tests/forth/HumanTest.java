package forth;

public class HumanTest {
    public static void main(String[] args) {
        Human h1 = new Human();
        h1.setHairColor("white");
        h1.setEyeColor("red");

        System.out.println(h1);

        h1.getLeftHand().setBroken(true);
        h1.getLeftHand().setBroken(false);
    }
}
